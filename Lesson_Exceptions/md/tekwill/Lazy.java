/**
 * Solution 2
 */
package md.tekwill;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Lazy {
    private ArrayList<LocalDate> list = new ArrayList<>();

    Lazy() {
        list.add(LocalDate.of(1992, 10, 23));
    }


    LocalDate getItem(int index) {
        System.out.println(list.get(index));
        return list.get(index);
    }


    //wasted so much time trying to create a checked ParseException, but this one is a runtime exception instead
    void addDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
        LocalDate newDate = (LocalDate) formatter.parse(date);
        list.add(newDate);
    }

    //checked exceptions do not let the program run , until handled
    public static Number parseReading(String reading) {
        if (reading == null)
            return null;
        NumberFormat numberFormat = NumberFormat.getInstance();
        try {
            return numberFormat.parse(reading);
        } catch (ParseException e) {
            return null;
        }

    }

    void readFile() throws FileNotFoundException {
        File file = new File("file.txt");
        FileInputStream stream = new FileInputStream(file);
    }


}
