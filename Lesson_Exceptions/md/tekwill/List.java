/**
 * Solution 4. It's nice that you can convert so easily between the collections.
 */
package md.tekwill;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class List {

    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<>();
        names.add("Liza");
        names.add("Jana");
        names.add("Mariana");
        names.add("Jana");
        System.out.println(names.size());
        if (names.contains("Jana")) {
            names.remove("Jana");
        }
        for (String name : names
        ) {
            System.out.println(name);

        }

        LinkedList linkedNames = new LinkedList(names);
        linkedNames.add("Xenia");
        linkedNames.add(2, "Iuliana");
        linkedNames.addLast("Mariana");
        linkedNames.addFirst("Iuliana");
        linkedNames.removeFirstOccurrence("Mariana");
        System.out.println(linkedNames);

        HashSet<String> hashNames = new HashSet<>(linkedNames);
        System.out.println(hashNames);
        hashNames.clear();

        LinkedHashSet<String> linkedHashNames = new LinkedHashSet<>();
        if (!linkedHashNames.isEmpty()) {
            linkedHashNames.addAll(hashNames);
        } else {
            linkedHashNames.addAll(names);
        }
        System.out.println(linkedHashNames);


    }
}
