/**
 * Solution 3
 */
package md.tekwill;

public class InvalidNameFormatException extends Exception {
    public InvalidNameFormatException(String errorMessage) {
        super(errorMessage);
    }
}
