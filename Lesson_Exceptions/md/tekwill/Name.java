/**
 * Solution 3
 */
package md.tekwill;

import java.util.Scanner;

public class Name {


    public String requestName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce name: ");
        String input = scanner.nextLine();
        return input;
    }

    public void validateName(String name) throws InvalidNameFormatException {
        if (!checkName(name)) {
            throw new InvalidNameFormatException("Invalid name format!");
        }
    }


    //the 2 hours i spend on this method a month ago all worth it , i'm reusing it the 5th time already
    private boolean checkName(String name) {
        boolean status = true;
        char[] charName = name.toCharArray();
        for (int i = 0; i < charName.length; i++) {
            byte x = (byte) charName[i];
            if ((x < 65) || (x > 122) || ((x < 97) && (x > 90))) {
                status = false;
                break;
            }
        }
        return status;
    }
}
