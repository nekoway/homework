/**
 * Solution 1
 * Still not sure what is the practical use of exceptions , unless my methods will be used by other programmers.
 * I can avoid exceptions using simple code without exceptions. Is this for people who error handle later?
 * Should i think of exceptions that a method can create from the get go and declare them?
 * "If a client can reasonably be expected to recover from an exception, make it a checked exception.
 * If a client cannot do anything to recover from the exception, make it an unchecked exception" from
 * Java Documents is even more confusing O_o
 */
package md.tekwill;

import java.io.FileNotFoundException;

public class WhyULying {
    int value = 0;

    WhyULying() {
    }

    //it seems, i can declare whatever i want here, do not need to be true
    // it lists all the unchecked exceptions as possible to be thrown
    //i guess IntelliJ does not know what kind of insane person is using it, everything is possible :)
    WhyULying(int value) throws ArithmeticException, FileNotFoundException, IndexOutOfBoundsException {
        this.value = value;
    }

    boolean isOddOrEven(int value) {
        if (value % 2 == 0) {
            System.out.println("even value");
            return true;
        } else {
            System.out.println("odd value");
            return false;
        }
    }


    // it asks me to handle the checked exception even if it's never thrown , not the unchecked one though .
    public static void main(String[] args) {
        try {
            WhyULying why = new WhyULying(23);
            why.isOddOrEven(why.value);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
