/**
 * Pokemoonn, no , Exceptiooooons, you gotta catch them all~~~!!
 */
package md.tekwill;

import java.io.FileNotFoundException;
import java.time.format.DateTimeParseException;

public class Test {
    public static void main(String[] args) {
        Name name = new Name();
        try {
            name.validateName(name.requestName());
        } catch (InvalidNameFormatException e) {
            System.out.println(e.getMessage());
        }

        Lazy lazy = new Lazy();
        try {
            lazy.getItem(1);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Index Out Of Bounds!");
            try {
                lazy.addDate("05/02/1992");
            } catch (DateTimeParseException e2) {
                System.out.println("Date is incorrect!");
            }

            try {
                lazy.readFile();
            } catch (FileNotFoundException e3) {
                System.out.println("File not found!");
            }
        }


    }
}
