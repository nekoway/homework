package md.tekwill.test;

import md.tekwill.models.Cell;
import md.tekwill.models.Virus;

public class Fight_for_life_or_death {
    public static void main(String[] args) {
        String[] dna= {"1","2","3"};
        Cell cell=new Cell(dna);
        Virus virus1=new Virus("virus");
//        cell.destroyNotSelf(virus1);  does not work in the way i expected it, need to still work on it
        cell.printDna();
        virus1.mimicHost(cell.getInsideCell());
        virus1.reproduce(cell.getDna());
        cell.printDna();
    }
}
