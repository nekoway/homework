/**
 * I admit. Some reverse engineering going on here. Solution 1.1
 */
package md.tekwill.dao;
import md.tekwill.models.Book;

public abstract class BookDao {

    public abstract void save(int id,Book book);
    public abstract Book  read(int id);
    public  abstract void delete(int id);

}
