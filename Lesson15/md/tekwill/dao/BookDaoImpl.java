/**
 * Solution 1.1
 */
package md.tekwill.dao;

import md.tekwill.models.Book;
import md.tekwill.models.Library;

public class BookDaoImpl extends BookDao {

    @Override
    public void save(int id, Book book) {
        Library.getLibrary()[id] = book;
    }

    @Override
    public void delete(int id) {
        Library.getLibrary()[id] = null;
    }

    public void updateAuthorName(int id, String authorName) {
        Library.getLibrary()[id].setAuthorName(authorName);
    }

    public void updateAuthorSurname(int id, String authorSurname) {
        Library.getLibrary()[id].setAuthorSurname(authorSurname);
    }

    public void updateBookName(int id, String bookName) {
        Library.getLibrary()[id].setBookName(bookName);
    }

    @Override
    public Book read(int id) {
        return Library.getLibrary()[id];
    }

}
