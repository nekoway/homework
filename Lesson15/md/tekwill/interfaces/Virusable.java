/**
 * solution 1.3 and 2.1,
 */
package md.tekwill.interfaces;

public interface Virusable extends Reproducible, Protectable {

    void mimicHost(String[] insideCell);
}
