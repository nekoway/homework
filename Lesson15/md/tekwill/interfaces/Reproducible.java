/**
 * It's a miotic reproduction, so no need for partner.
 */
package md.tekwill.interfaces;

public interface Reproducible {

    Object reproduce(String[] dna);
}
