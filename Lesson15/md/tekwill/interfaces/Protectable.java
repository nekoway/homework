package md.tekwill.interfaces;

public interface Protectable {

    boolean isSelf(Object object);
    void destroyNotSelf(Object object);
}
