package md.tekwill.models;

import md.tekwill.interfaces.Virusable;

public class Virus implements Virusable {

    private String dna; // is so small that it doesn't need an array
    private String protective_layer = "My protective layer";

   public Virus(String dna){
        this.dna=dna;
    }

    @Override
    public String[] reproduce(String[] dna) {
        for (int i = 0; i < dna.length; i++) {
            dna[i]=this.dna;
        }
        System.out.println("Your cell is taken over. Muahaha!");
        return dna;
    }

    @Override
    public boolean isSelf(Object object) {
        boolean status=false;
        if ( object.equals(this.dna)) {
            status=true;
        } else {
            status=false;
        }
        return status;
    }

    //not sure what to write it , it's not the virus primary goal ,it's a consequence of reproduction.
    @Override
    public void destroyNotSelf(Object object) {
        if (!isSelf(object)){
        }

    }

    @Override
    public void mimicHost(String[] insideCell) {
        for (int i = 0; i < insideCell.length; i++) {
            if (insideCell[i].equals("cell protective layer")){
                this.protective_layer = "cell protective layer";
            }
        }
    }

    public String getProtective_layer() {
        return protective_layer;
    }
}
