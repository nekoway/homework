package md.tekwill.models;

public class Book {

    private int bookId;
    private String authorName;
    private String authorSurname;
    private String bookName;
    private String bookDescription; // not sure if i will need it yet.
    
    public Book(String authorName,String authorSurname,String bookName){
        this.authorName=authorName;
        this.authorSurname=authorSurname;
        this.bookName=bookName;
        Library.nrOfBooks++; //maybe will change location later
    }

    public int getBookId() {
        return bookId;
    }

    //not sure if i want to give this option to anybody.The id is the way i'm retrieving info.
//    public void setBookId(int bookId) {
//        this.bookId = bookId;
//    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }
}
