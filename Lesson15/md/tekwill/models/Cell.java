/**
 * Solution 1.2 and 2.2. My imagination is running low on real life applications, so i will tackle this abstract
 * homework with even more abstractions.
 */
package md.tekwill.models;

import md.tekwill.interfaces.Protectable;
import md.tekwill.interfaces.Reproducible;

public class Cell implements Reproducible, Protectable {

    private String[] dna;
    //it's not just cell organels that are supposed to be in a cell, but whatever.
    private String[] insideCell = {"ribosoms", "mitochondria", "apparatus Golgi", "my other organels","cell protective layer"};

    @Override
    public Cell reproduce(String[] dna) {
        Cell daughterCell =new Cell(dna);
        System.out.println("Another cell is created");
        return daughterCell;
    }

    @Override
    public boolean isSelf(Object object) {
        boolean status=true;
        for (int i = 0; i <insideCell.length; i++) {
            if(object.equals(insideCell[i])){
                status=true;
            } else {
                status=false;
                break;
            }
        }
        return status;
    }

    @Override
    public void destroyNotSelf(Object object) {
        if(!isSelf(object)){
            object=null;
            System.out.println("Foreign object destroyed");
        }

    }

    public Cell(String[]dna){
        this.dna=dna;
    }

    public String[] getDna() {
        return dna;
    }

    public String[] getInsideCell() {
        return insideCell;
    }

    public void printDna(){
        for (int i = 0; i <this.dna.length ; i++) {
            System.out.println(dna[i]);
        }
    }
}
