public class Odds {
    public static void main(String[] args) {
        int n = 5;
        int x = 0;
        while (x < n) {
            if (x % 2 != 0) {
                System.out.print(x);
                System.out.print(",");
            }
            x++;
        }

    }
}
