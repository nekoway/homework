public class Sum {
    public static void main(String[] args) {
        int n = 5;
        int x = 0;
        int sum = 0;
        while (x < n) {
            sum = sum + x;
            x++;
        }
        System.out.println(" The Sum is: " + sum);
    }
}
