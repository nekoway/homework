/**
 * This class has methods for editing information about the book, according to my arbitrary requirements. To change the
 * name of the author to uppercase , check if the name contains only letters, change the surname to an initial and
 * change the first letter in the name of the book to uppercase. Second solution.
 */
package md.tekwill.library;

public class NameEditor {
    String name = "smith";     //needed to add values , because Null Pointer Exception Error :(
    String surname = "jhon";
    String bookName = "no way";
    String info;
    char initial;

    public NameEditor() {
    }

    public void upperName() {
        name = name.toUpperCase();
    }

    public char changeToInitial(String surname) {
        surname = surname.toUpperCase();
        char x = surname.charAt(0);
        return x;
    }

    public boolean checkName(String name) {  // jesus , this method took me 3 hours, started adding symbols by hand , then in the middle of it realise there is ascii ,
        boolean status = true;     // and that probably the letters are close to each other, then realise that Java does not convert to ascii , need to parse it
        char[] charName = name.toCharArray();  // then realise that it didn't take into account the final char , because charName.length-1 , i will remember
        for (int i = 0; i < charName.length; i++) {   // this method forever.
            byte x = (byte) charName[i];
            if ((x <= 65) || (x >= 122) || ((x < 97) && (x > 90))) {
                status = false;
                break;
            }
        }
        return status;
    }

    public void correctBookName() {
        char firstLetter = changeToInitial(bookName);
        bookName = bookName.replace(bookName.charAt(0), firstLetter);
    }
}