/**
 * Third solution.
 */
package md.tekwill.library;

public class Book {
    private static int counter = 0;

    public Book() {
        counter++;
    }

    NameEditor book = new NameEditor();

    public static int getCounter() {
        return counter;
    }

    public String getName() {
        return book.name;
    }

    public void setName(String authorName) {
        boolean status = book.checkName(authorName);
        if (status == true) {
            book.name = authorName;
        } else {
            System.out.println("Invalid value. The author's name must contain only letters.Try again.");
        }
    }

    public String getSurname() {
        return book.surname;
    }

    public void setSurname(String authorSurname) {
        boolean status = book.checkName(authorSurname);
        if (status == true) {
            book.surname = authorSurname;
        } else {
            System.out.println("Invalid value. The author's surname must contain only letters.Try again.");
        }
    }

    public String getBookName() {
        return book.bookName;
    }

    public void setBookName(String bookName) {
        book.bookName = bookName;
    }

    public String getInfo() {   //how do i use " in a String?
        book.correctBookName();
        char i = book.changeToInitial(book.surname);
        book.upperName();
        book.info = "'" + book.bookName + "' by " + i + ". " + book.name;
        return book.info;
    }

}
