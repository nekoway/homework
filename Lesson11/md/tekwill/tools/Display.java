/**
 * The task was to create a realistic class. What is reality anyway? Reality is something subjective . Am i real?
 * Are you, the person reading it real? I'm with Descartes on this one. " I think, therefore i exist". Even in
 * mathematics, there are "real" numbers , who contain irrational numbers , that we get using very abstract concepts.
 * The point is my class is real , even if it is abstract. Feel free to disagree. First solution.
 */
package md.tekwill.tools;

import md.tekwill.library.Book;

public class Display {

    Book library[];

    public Display() { // Why do we need to do this again?

    }

    public static void nrOfBooks() {
        int counter = Book.getCounter();
        if (counter == 0) {
            System.out.println("There is " + counter + " book in the library.");

        } else {
            System.out.println("There are " + counter + " books in the library.");
        }
    }

    public static void printBookInfo(String info) {
        System.out.println(info);
    }

    // Wanted to make some kind of sorting method or one that adds the Book objects to an array. Turns out it's complicated.
    // No inspiration for a realistic method with my current knowledge. Need to add something later.


}