package md.tekwill.test;

import md.tekwill.library.Book;
import md.tekwill.tools.Display;

public class Test {
    public static void main(String[] args) {
        Book book1 = new Book();
        book1.setName("Rowling");
        book1.setSurname("Joanne");
        book1.setBookName("harry potter");

        Book book2 = new Book();
        book2.setName("Mitchell");
        book2.setSurname("Margaret");
        book2.setBookName("Gone with the wind");
        book2.setName("gh7");
        System.out.println(book2.getName());

        Book book3 = new Book();
        book3.setSurname("h jdk");
        System.out.println(book3.getSurname());

        Display.nrOfBooks();
        Display.printBookInfo(book1.getInfo());
        Display.printBookInfo(book2.getInfo());
        Display.printBookInfo(book3.getInfo());

    }

}
