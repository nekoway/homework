/**
 * Originally, wanted to do something more complicated. A more difficult authentication process, check
 * for no whitespace in username, no username that is already in use, requirements for password,
 * if you write your password wrong 3 times, a security question, and a password reset and interdiction
 * to use the same password after a reset. Will expand this later , when it will be more in the line
 * with scope of the homework.
 */
package md.tekwill.user;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class User {

    private String username;
    private LocalDate birthday;
    private String password;
    private static int counter=0;

    // solution 7

    static {
        getCounter();
        counter++;
    }
    {
        System.out.println("");
        System.out.println("A new account was created.");
    }


    public static void getCounter() {
        System.out.println("There are currently: "+counter+" accounts.");
    }

    {
        System.out.println("Test nr.2.");
    }


    // solution nr. 3

    public User(String username) {
        this.username = username;
        System.out.println("Need to add a password.");
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, LocalDate birthday) {
        this.username = username;
        this.password = password;
        this.birthday = birthday;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    //solution 2

    public void displayUserInfo(String username) {
        System.out.println("Your username is: " + username);
    }

    public void displayUserInfo(String username, LocalDate birthday) {
        this.displayUserInfo(username);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d.M.yyyy");
        System.out.println("Your birthday is: " + dtf.format(birthday));
    }

    //need to change this later, this info should be only available to the user after authentication,
    // or after a reset.

    public void displayUserInfo(String username, LocalDate birthday, String password) {
        this.displayUserInfo(username, birthday);
        System.out.println("Your password is: " + password);
    }

    // solution 7

    {
        System.out.println("Test nr.3");
    }

    static {
        getCounter();
        System.out.println("This is for test only.");
    }

}
