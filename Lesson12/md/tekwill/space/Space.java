/**
 * What is the purpose of this class? I have no idea. A game maybe?
 */
package md.tekwill.space;

import java.util.Scanner;

public class Space {

    private static int length = 1; // Need to read more about how to avoid the NullPointerException Error.
    private static int width = 1;  // Had to modify a lot of code to avoid it.
    private static int height = 1;

    // solution nr.4

    Space(int length) {
        this.length = length;
        announcement(this); // solution nr. 5

    }

    Space(int length, int width) {
        this(length);
        this.width = width;
    }

    public Space(int length, int width, int height) { //needed to make it public for the test to work
        this(length, width);                          // not sure that i want to make it public though
        this.height = height;
    }

    // solution nr.5

    private void announcement(Space space) {
        System.out.println("A space was created");
    }

    public static int getLength() {
        return length;
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    // solution 1

    public void display() {
        int area = this.calculateArea();
        System.out.println("The area is: " + area + " square meters");
        int volume = this.calculateVolume();
        System.out.println("The volume is: " + volume + " cubic meters");
    }

    public int calculateArea() {
        int area = length * width;
        return area;
    }

    public int calculateVolume() {
        int volume = this.calculateArea() * height;
        return volume;
    }

    // solution 6

    public Space getStats() {
        return this;
    }

    public static void createSpace() {
        System.out.println("");
        System.out.println("What kind of space to you want to generate?");
        System.out.println("Press 1 - for one-dimensional space.");
        System.out.println("Press 2 - for two-dimensional space.");
        System.out.println("Press 3 - for three-dimensional space.");

        Scanner scanner = new Scanner(System.in); // Very cool class. Was waiting for something like this.
        System.out.println("Write your answer here:"); // Still have no idea how can i connect this
        String s1 = scanner.nextLine();                // backend stuff to some GUI.
        if (s1.equals("1")) {
            Space space = new Space(length);
        } else {
            if (s1.equals("2")) {
                Space space = new Space(length, width);
            } else {
                if (s1.equals("3")) {
                    Space space = new Space(length, width, height);
                } else {
                    System.out.println("Invalid answer.Failed to create a space");
                }
            }
        }

    }


}
