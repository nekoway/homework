/** I get the exercise nr.7 now. The static blocks are initialised first ,regardless of their location in the code.
 * Here we have the first , then the second static block message. Then the message from the instance blocks
 * in their respective order. Then the message from the instance class creation. Still need to think about a
 * practical use of this information.
 */
package md.tekwill.test;
import md.tekwill.space.Space;
import  md.tekwill.user.User;

import java.time.LocalDate;

public class Test {

    public static void main(String[] args) {

        User user = new User("Nekoway","jana231092");
        user.setBirthday(LocalDate.of(1992,10,23));
        user.displayUserInfo(user.getUsername(),user.getBirthday(),user.getPassword());
        User user2 = new User("Inuway");

        Space.createSpace();
        new Space(10,40,5).getStats().display();

    }


}
