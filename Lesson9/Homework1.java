public class Homework1 {
    void sum(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            sum = sum + i;
        }
        System.out.println(" The Sum is: " + sum);
    }

    void evens(int n) {
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                System.out.print(i);
                System.out.print(",");
            }
        }
    }

    void hello(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("Hello");
        }
    }

    void odds(int n) {
        for (int i = 0; i < n; i++) {
            if (i % 2 != 0) {
                System.out.print(i);
                System.out.print(",");
            }
        }
    }
}
