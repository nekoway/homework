package md.tekwill.blueprint;

import md.tekwill.size.LotSize;

public class LotSizeTest {
    public static void main(String[] args) {

        LotSize lot1 = new LotSize();
        int x = lot1.getWidth();
        System.out.println(x + "m");

        lot1.setWidth(10);
        lot1.setLength(20);
        lot1.insideCity=false;
        //  lot1.getDescription();  This one failed miserably, needed to add a public method.
        lot1.seeDescription();
        //   lot1.getSize();// so , if the object is not inside the package , still no luck , even with default method.
        lot1.writeSize(); // well, created a new public method , that used the existing default one. good to know.
        //lot1.getLocation(); cannot use a method from another class, even if in the same package. Is there some workaround?
        boolean n = lot1.insideCity;
        System.out.println(n);

        HouseSize house1 = new HouseSize();
        house1.getLocation(); // but can access a class and it's default method if in the same package.
        int i = house1.getNrOfLevels();
        System.out.println(i+" level");
        house1.setNrOfLevels(3);
        house1.setLength(5);
        house1.setWidth(10);
        house1.getSize();
    }


}

