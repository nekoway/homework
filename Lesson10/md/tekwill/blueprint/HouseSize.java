/**
 * This is the second homework.Too lazy to copy methods , so i used inheritance.
 * And in my virtual world there are only perfectly rectangular houses with maximum 4 levels.
 */
package md.tekwill.blueprint;

public class HouseSize extends md.tekwill.size.LotSize {
    private int length = 1;
    private int width = 1;
    private int nrOfLevels = 1;

    public int getNrOfLevels() {
        return nrOfLevels;
    }

    public int setNrOfLevels(int n) {
        if ((n > 0) && (n <= 4)) {
            nrOfLevels = n;
            return nrOfLevels;
        } else {
            System.out.println("This is an invalid number of levels.The value should be from 1 to 4. Try again.");
            return nrOfLevels;
        }

    }

    private int totalArea() { //Could of used a for loop, would be shorter , but wanted to practice switch.
        int area = 0;
        switch (nrOfLevels) {
            case 1:
                area = lotAreaCalculator();
                break;
            case 2:
                area = 2 * lotAreaCalculator();
                break;
            case 3:
                area = 3 * lotAreaCalculator();
                break;
            case 4:
                area = 4 * lotAreaCalculator();
                break;
        }
        return area;
    }

    public void getLocation() {
        if (insideCity == false) {
            System.out.println("This property is outside the city area");
        } else {
            System.out.println("This property is inside the city");
        }
    }

    String getSize() {  // is this overriding? i guess. The area of the house should take into account all the levels.
        int n = totalArea();
        if (n <= 40) {
            size = "Small";
        } else if ((n > 40) && (n <= 70)) {
            size = "Medium";
        } else {
            size = "Large";
        }
        return size;

    }

    public void seeHouseSize() { //added a new method to have access to the default one
        String n = getSize();
        System.out.println(size);
    }

}
