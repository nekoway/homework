/**
 * In my world there are only rectangular lots , with no weird shapes , and only integer values. This is the bonus
 * homework.
 */
package md.tekwill.size;

public class LotSize {
    private int length = 1;
    private int width = 1;
    public boolean insideCity;
    public String size; //had to add after testing
    public String description; // had to add after testing

    public int getLength() {
        return length;
    }

    public int setLength(int n) {
        if (n > 0) {
            length = n;
            return length;
        } else {
            System.out.println("This is an invalid length value.The value should be positive. Try again.");
            return length;
        }

    }

    public int getWidth() {
        return width;
    }

    public int setWidth(int n) {
        if (n > 0) {
            width = n;
            return width;
        } else {
            System.out.println("This is an invalid width value.The value should be positive. Try again.");
            return width;
        }

    }

    private String getDescription() {
        String description = "This is a " + length + " x " + width + " lot.";
        System.out.println(description);
        return description;
    }

    public void seeDescription() { // needed to add this, to make the private one work
        description = getDescription();
    }


    protected int lotAreaCalculator() { // I'm sure there already exists a public java method , but whatever.
        int area = length * width;
        return area;
    }

    String getSize() {
        int n = lotAreaCalculator();
        if (n <= 40) {
            size = "Small";
        } else if ((n > 40) && (n <= 70)) {
            size = "Medium";
        } else {
            size = "Large";
        }
        return size;

    }

    public void writeSize() {   // had to add a public method that used the default one
        String size = getSize();
        System.out.println(size);
    }
}