package md.tekwill.size;
import md.tekwill.price.ApartmentSize;

public class ApartmentSizeTest {
    public static void main(String[]args) {
        ApartmentSize apartment1= new ApartmentSize();
        apartment1.setPrice(-5);
        apartment1.setPrice(440);
        apartment1.setLength(5);
        apartment1.setWidth(5);
        apartment1.insideCity= true;
        int area = apartment1.lotAreaCalculator();
        System.out.println("The area of the apartment is "+area+" square meters.");
        apartment1.writeSize();
        apartment1.totalPrice();
        apartment1.seeDescription(); //is using the LotSize method, need to specify witch to use ,i guess
        apartment1.seeApartmentDescription(); //is using the ApartmentSize method , but the values are back to default
                                               // too tired to try to get to bottom of this, next time
        LotSize lot2 = new LotSize();
        lot2.seeDescription();
        lot2.lotAreaCalculator();
    }

}