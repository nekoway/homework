package md.tekwill.price;
import md.tekwill.blueprint.HouseSize;

public class HouseSizeTest {
    public static void main(String[]args) {

        HouseSize house2= new HouseSize();
        house2.insideCity=true;
        house2.setLength(20);
        house2.setWidth(3);
        house2.setNrOfLevels(2);
        house2.getLocation();
        house2.seeDescription();
        house2.seeHouseSize(); //modified to have access to the default method

        ApartmentSize apartment2= new ApartmentSize();
        apartment2.setPrice(590);
        apartment2.setWidth(9);
        apartment2.setLength(7);
        apartment2.insideCity=true;
        apartment2.writeSize();
        apartment2.seeApartmentDescription();
        apartment2.totalPrice();
        apartment2.seeDescription();

    }
}
