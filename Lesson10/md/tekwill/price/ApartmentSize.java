/**
 * This is the second solution. A public method that uses 2 private ones.
 */
package md.tekwill.price;

public class ApartmentSize extends md.tekwill.size.LotSize {
    private int length = 1;
    private int width = 1;
    private int price = 1;

    public int getPrice() {
        return price;
    }


    public int setPrice(int n) {
        if (n > 0) {
            price = n;
            return price;
        } else {
            System.out.println("This is an invalid price value.The value should be positive. Try again.");
            return price;
        }

    }

    private int basePrice() {   // the base price
        int i = lotAreaCalculator() * price;
        return i;
    }

    private int addPrice() {    //+ 20% to the price if inside city
        if (insideCity == true) {
            return basePrice() * 20 / 100;
        } else {
            return 0;
        }
    }

    public void totalPrice() {
        int sum = basePrice() + addPrice();
        System.out.println("The total price for the property is : " + sum + " $.");
    }

    String getArea() {
        String description = "This is a lovely " + length + " x " + width + " apartment.";
        System.out.println(description);
        return description;
    }

    public void seeApartmentDescription() {
        description = getArea();

    }
}