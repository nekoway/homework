public class Haiku{
    
    public static void main(String[] args) {
        
        System.out.print("The dying season, ");
        System.out.print("autumn brings a blazing end, ");
        System.out.print("the long night begins.");

    }
}