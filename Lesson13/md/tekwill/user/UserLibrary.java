/**
 * Solution 1. Would be interesting to add a method for adding a book from the General Library to the User One.
 */
package md.tekwill.user;

import md.tekwill.library.Library;


public class UserLibrary extends Library {
    private String[][] userLibrary = new String[50][2];
    private int nrOfBooks = 0;

    public void displayNrOfUserBooks() { //i will remove the static modifiers later.
        System.out.println("There are a total of : " + nrOfBooks + " books");
    }


    @Override
    public int getNrOfBooks() {
        return nrOfBooks;
    }

    @Override
    public String[][] getLibrary() {
        return userLibrary;
    }

    @Override
    public void addBook(String[][] userLibrary) {
        super.addBook(this.userLibrary);
        Library.nrOfBooks--;  //not very elegant, but it works
        nrOfBooks++;
    }

    @Override
    public void displayLibrary() {
        System.out.println("");
        for (int i = 0; i < nrOfBooks; i++) {
            System.out.print(i + 1 + ". ");
            for (int j = 0; j < 2; j++) {
                System.out.println(userLibrary[i][j] + " ");
            }
        }

    }

}
