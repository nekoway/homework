/**
 * Well this was a journey. Started with 2 methods , had to break them into 4 , then into 6. Then added properties .
 * Didn't work otherwise. I understand now the need for breaking down your programs into smaller ones.
 * When i'm rerunning the program in the Test, the books are not saved in the array, even when i made the array static.
 * Not sure how to fix it yet. This library is for 100 books.
 */
package md.tekwill.library;

import java.util.Arrays;
import java.util.Scanner;

public class Library {

    private String authorName;
    private String authorSurname;
    protected static int nrOfBooks = 0;
    private static String[][] library = new String[100][2];
    NameEditor nameEditor = new NameEditor();

    public int getNrOfBooks() {
        return nrOfBooks;
    }

    void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String[][] getLibrary() {
        return library;
    }

    public static void displayNrOfBooks() {
        System.out.println("");
        System.out.println("There are a total of : " + nrOfBooks + " books");
    }

    String getAuthorName() {
        return this.authorName;
    }

    String getAuthorSurname() {
        return this.authorSurname;
    }


    String askAuthorName() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce the author's name: ");
        String authorName = sc.nextLine();
        return authorName;
    }


    String askAuthorSurname() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce the author's surname: ");
        String authorSurname = sc.nextLine();
        return authorSurname;
    }


    String getBookName() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce the book name: ");
        String bookName = sc.nextLine();
        return bookName;
    }

    void checkAuthorName() {
        String authorName = askAuthorName();
        if (nameEditor.checkName(authorName)) {
            setAuthorName(authorName);
        } else {
            System.out.println("Invalid name format.Need to contain only letters.Try again.");
            checkAuthorName();
        }
    }

    void checkAuthorSurname() {
        String authorSurname = askAuthorSurname();
        if (nameEditor.checkName(authorSurname)) {
            setAuthorSurname(authorSurname);
        } else {
            System.out.println("Invalid name format.Need to contain only letters.Try again.");
            checkAuthorSurname();
        }
    }

    // did give me a lot of headache, fixed by introducing a getNrOfBooks method. Worked fine in parent class,
    //but in child class was added in Arr[3][] ,instead of Arr[1], because of the nr.of books in parent class.
    //Thank god for the debugging feature. Need to take into account that my class may be inherited in the future.
    public void addBook(String[][] library) {
        int i = getNrOfBooks();
        System.out.println("");
        System.out.println("New Book:");
        checkAuthorName();
        checkAuthorSurname();
        String bookName = nameEditor.correctBookName(getBookName());
        System.out.println("");
        String name = getAuthorName().toUpperCase();
        char surname = nameEditor.changeToInitial(getAuthorSurname());
        String fullName = name + " " + surname + ". ";
        library[i][0] = fullName;
        library[i][1] = bookName;
        nrOfBooks++;
    }

    // does not work , ClassCastException. Maybe because it's a 2D array.
    public void sortByAuthor() {
        Arrays.sort(library);
    }

    // if(==null){break;) didn't work, displayed all the books with null values, used nrOfBooks instead
    public void displayLibrary() {
        System.out.println("");
        for (int i = 0; i < nrOfBooks; i++) {
            System.out.print(i + 1 + ". ");
            for (int j = 0; j < 2; j++) {
                System.out.println(library[i][j] + " ");
            }
        }

    }
}


