/**
 * Would be great if i can make the sorting work. Maybe add some find , edit , delete features.
 */
package md.tekwill.test;

import md.tekwill.library.Library;
import md.tekwill.user.UserLibrary;

public class Test {
    public static void main(String[] args) {
        Library library=new Library();
        library.addBook(library.getLibrary());
        library.addBook(library.getLibrary());
        library.displayLibrary();
        library.addBook(library.getLibrary());
        library.displayLibrary();


        UserLibrary myLibrary=new UserLibrary();
        myLibrary.addBook(myLibrary.getLibrary());
        myLibrary.displayLibrary();
        Library.displayNrOfBooks();
        myLibrary.displayNrOfUserBooks();
    }
}
