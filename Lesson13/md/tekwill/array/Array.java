package md.tekwill.array;

public class Array {

    public int intArray[];

    void findIndex(int intArray[], int value) {
        int match = 0;
        for (int i = 0; i < intArray.length; i++) {
            if (value == intArray[i]) {
                System.out.println("Index position of " + value + " is : " + i);
                match++;
            }
        }
        if (match == 0) {
            System.out.println("Such value does not exist in this array");
        }
    }

// need to think of some way to not display the same info maybe
    void findDuplicates(int intArray[]) {
        System.out.println("");
        for (int i = 0; i < intArray.length; i++) {
            int counter = 0;
            String index = " at index ";
            for (int j = 0; j < intArray.length; j++) {
                if (intArray[i] == intArray[j]) {
                    if (i == j) {
                        continue;
                    }
                    String j1 = Integer.toString(j);
                    counter++;
                    index = index.concat(j1).concat(" ");
                }
            }
            switch (counter) {
                case 1:
                    System.out.println("There is " + counter + " duplicate value for " + intArray[i] + index);
                    break;
                case 0:
                    System.out.println("There are " + counter + " duplicate values for " + intArray[i]);
                    break;
                default:
                    System.out.println("There are " + counter + " duplicate values for " + intArray[i] + index);
                    break;

            }

        }
    }

    void reverseArray(int intArray[]) {
        int arr[]=new int[intArray.length];
        for (int i = 0; i <intArray.length; i++) {
            arr[i]=intArray[intArray.length-1-i];
        }
        for (int i = 0; i <intArray.length ; i++) {
            intArray[i]=arr[i];
        }
    }

    void print(int intArray[]){
        System.out.println("");
        for (int i = 0; i < intArray.length; i++) {
            System.out.print(intArray[i]+" ");
        }
    }

}
