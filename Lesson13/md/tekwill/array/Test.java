package md.tekwill.array;

public class Test {
    public static void main(String[] args) {
        Array array = new Array();
        array.intArray = new int[]{12, 3, 5, 23, 8, 12, 3, 2, 2, 2};
        array.findIndex(array.intArray, 12);
        array.findDuplicates(array.intArray);
        array.reverseArray(array.intArray);
        array.print(array.intArray);
    }
}

