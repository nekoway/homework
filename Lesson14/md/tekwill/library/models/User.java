/**
 * not sure if i will use it yet. It's here if i want to add a UserLibrary in the future. Or for login.
 */
package md.tekwill.models;

public class User {

    private int userId;
    private String name;
    private String surname;

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
