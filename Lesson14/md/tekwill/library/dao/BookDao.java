package md.tekwill.library.dao;

import md.tekwill.library.models.Book;
import md.tekwill.library.models.Library;

public class BookDao {

    // i need an id to know where to save the book in the array. Is there an easier way?
    public void save(int id, Book book) {
        Library.getLibrary()[id] = book;
    }

    public void delete(int id) {
        Library.getLibrary()[id] = null;
    }

    public void updateAuthorName(int id, String authorName) {
        Library.getLibrary()[id].setAuthorName(authorName);
    }

    public void updateAuthorSurname(int id, String authorSurname) {
        Library.getLibrary()[id].setAuthorSurname(authorSurname);
    }

    public void updateBookName(int id,String bookName) {
        Library.getLibrary()[id].setBookName(bookName);
    }

    public Book read(int id) {
        return Library.getLibrary()[id];
    }

}
