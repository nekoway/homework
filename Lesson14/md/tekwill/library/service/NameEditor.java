/**
 * Still reusing my old homework.
 * This class has methods for editing information about the book, according to my arbitrary requirements. To change the
 * name of the author to uppercase , check if the name contains only letters, change the surname to an initial and
 * change the first letter in the name of the book to uppercase.
 */
package md.tekwill.service;

public class NameEditor {
    String name = "smith";     //needed to add values , because Null Pointer Exception Error :(
    String surname = "jhon";
    String bookName = "no way";
    String info;
    char initial;

    public NameEditor() {
    }

    public void upperName() {
        name = name.toUpperCase();
    }

    public char changeToInitial(String surname) {
        surname = surname.toUpperCase();
        char x = surname.charAt(0);
        return x;
    }

    public boolean checkName(String name) {  // realised during testing that i need to put x<65 and not x<=65
        boolean status = true;               // thanks Hans Christian Anderson ,the program didn't take into account A
        char[] charName = name.toCharArray();
        for (int i = 0; i < charName.length; i++) {
            byte x = (byte) charName[i];
            if ((x < 65) || (x > 122) || ((x < 97) && (x > 90))) {
                status = false;
                break;
            }
        }
        return status;
    }

    //Found out how to use " in Strings , \", useful to know. Need to work some more on this one.
    //It replaces all the char values that matches with the first one with UpperCase, found out during testing.

    public String correctBookName(String bookName) {
//        char firstLetter = changeToInitial(bookName);
//        bookName = bookName.replace(bookName.charAt(0), firstLetter);
        bookName = "\"" + bookName + "\"";
        return bookName;
    }
}